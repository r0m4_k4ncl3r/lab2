#include "form4.h"
#include "ui_form4.h"

Form4::Form4(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form4)
{
    ui->setupUi(this);
}

Form4::~Form4()
{
    delete ui;
}

void Form4::on_pushButton_clicked()
{
    // Обработка кнопки "Сформировать"

    // Создание ссылки на первую форму
    Form *form = new Form();

    // Вызов функции из первой формы
    form->replace_word_in_doc("premia.docx", ui->lineEdit->text(), "{orgname}");
    form->replace_word_in_doc("premia.docx", ui->lineEdit_2->text(), "{code}");
    form->replace_word_in_doc("premia.docx", ui->lineEdit_3->text(), "{coder}");
    form->replace_word_in_doc("premia.docx", ui->lineEdit_4->text(), "{docnumber}");
    form->replace_word_in_doc("premia.docx", ui->lineEdit_5->text(), "{dirname}");
    form->replace_word_in_doc("premia.docx", ui->dateEdit->text(), "{sosdata}");
    form->replace_word_in_doc("premia.docx", ui->lineEdit_6->text(), "{name}");
    form->replace_word_in_doc("premia.docx", ui->lineEdit_7->text(), "{strpod}");
    form->replace_word_in_doc("premia.docx", ui->lineEdit_8->text(), "{dolzh}");
    form->replace_word_in_doc("premia.docx", ui->lineEdit_9->text(), "{motiv}");
    form->replace_word_in_doc("premia.docx", ui->lineEdit_10->text(), "{gift}");
    form->replace_word_in_doc("premia.docx", ui->lineEdit_11->text(), "{osnov}");
    form->replace_word_in_doc("premia.docx", ui->lineEdit_12->text(), "{tabel}");

    close();
}


void Form4::on_pushButton_2_clicked()
{
    // Обработка кнопки "Отменить"

    close();
}

