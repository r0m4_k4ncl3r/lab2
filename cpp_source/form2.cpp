#include "form2.h"
#include "ui_form2.h"

Form2::Form2(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form2)
{
    ui->setupUi(this);
}

Form2::~Form2()
{
    delete ui;
}

void Form2::on_pushButton_clicked()
{
    // Обработка кнопки "Сформировать"

    // Создание ссылки на первую форму
    Form *form = new Form();

    // Вызов функции из первой формы
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit->text(), "{orgname}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_2->text(), "{code}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_3->text(), "{coder}");
    form->replace_word_in_doc("kartochka.docx", ui->dateEdit->text(), "{datefirst}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_7->text(), "{tabnum}");
    form->replace_word_in_doc("kartochka.docx", ui->dateEdit_2->text(), "{datethird}");
    form->replace_word_in_doc("kartochka.docx", ui->dateEdit_3->text(), "{datesec}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_5->text(), "{inn}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_6->text(), "{strsvid}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_8->text(), "{a}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_9->text(), "{rabota}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_10->text(), "{vidraboti}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_11->text(), "{pol}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_12->text(), "{nomer}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_13->text(), "{surname}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_14->text(), "{name}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_15->text(), "{fathername}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_16->text(), "{birthplace}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_17->text(), "{citizenship}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_18->text(), "{language}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_19->text(), "{level}");
    form->replace_word_in_doc("kartochka.docx", ui->lineEdit_20->text(), "{education}");

    close();
}


void Form2::on_pushButton_2_clicked()
{
    // Обработка кнопки "Отменить"

    close();
}

