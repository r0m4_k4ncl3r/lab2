#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    // открытие окна для заполнения шаблона штатного расписания
    Form *form = new Form();
    form->show();
}


void MainWindow::on_pushButton_2_clicked()
{
    // открытие окна для заполнения шаблона линия карточки сотрудника
    Form2 *form = new Form2();
    form->show();
}


void MainWindow::on_pushButton_3_clicked()
{
    // открытие окна для заполнения шаблона заявления на отпуск
    Form3 *form = new Form3();
    form->show();
}


void MainWindow::on_pushButton_4_clicked()
{
    // открытие окна для заполнения шаблона премия сотруднику
    Form4 *form = new Form4();
    form->show();
}

