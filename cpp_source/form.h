#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include <qdebug.h>
#include <unistd.h>

namespace Ui {
class Form;
}

class Form : public QWidget
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = nullptr);
    ~Form();
    void replace_word_in_doc(QString name_document, QString word, QString word_in_doc);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::Form *ui;
};

#endif // FORM_H
