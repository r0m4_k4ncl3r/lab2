#include "form3.h"
#include "ui_form3.h"

Form3::Form3(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form3)
{
    ui->setupUi(this);
}

Form3::~Form3()
{
    delete ui;
}

void Form3::on_pushButton_clicked()
{
    // Обработка кнопки "Сформировать"

    // Создание ссылки на первую форму
    Form *form = new Form();

    // Вызов функции из первой формы
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit->text(), "{orgname}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_2->text(), "{code}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_3->text(), "{coder}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_4->text(), "{dirname}");
    form->replace_word_in_doc("otpusk.docx", ui->dateEdit->text(), "{sosdata}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_8->text(), "{name}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_7->text(), "{strpod}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_9->text(), "{dolzh}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_5->text(), "{docnumber}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_6->text(), "{tabel}");
    form->replace_word_in_doc("otpusk.docx", ui->dateEdit_2->text(), "{dataone}");
    form->replace_word_in_doc("otpusk.docx", ui->dateEdit_3->text(), "{datatwo}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_10->text(), "{fd}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_11->text(), "{datathree}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_12->text(), "{datafour}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_13->text(), "{document}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_14->text(), "{skd}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_15->text(), "{datafive}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_16->text(), "{datasix}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_17->text(), "{a}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_18->text(), "{dataseven}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_19->text(), "{dataeight}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_20->text(), "{osndoc}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_21->text(), "{datanine}");
    form->replace_word_in_doc("otpusk.docx", ui->lineEdit_22->text(), "{number}");

    close();
}

void Form3::on_pushButton_2_clicked()
{
    // Обработка кнопки "Отменить"

    close();
}

