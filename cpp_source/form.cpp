#include "form.h"
#include "ui_form.h"

#include <QProcess>

Form::Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);
}

Form::~Form()
{
    delete ui;
}

void Form::replace_word_in_doc(QString name_document, QString word, QString word_in_doc)
{
    // Функция для вызова python утилиты, которая будет подставлять слово в шаблон

    // Команда, которая будет выполняться, а именно python
    QString command("./venv/bin/python");

    // Аргументы для python
    QStringList arguments;
    arguments << "./replace_word_in_doc.py" // python утилита
              << QString("\"./documents/%1\"").arg(name_document) // docx файл
              << QString("\"%1\"").arg(word) // слово, на которое нужно заменить
              << QString("\"%1\"").arg(word_in_doc); // заменяемое слово

    // Создание процесса
    QProcess *process = new QProcess(this);

    // Настройка вывода отладочной информации в файл
    process->setStandardOutputFile("output.txt");
    process->setStandardErrorFile("errors.txt");

    // Запуск команды с аргументами
    process->start(command, arguments);
    sleep(1); // Жесткий костыль)))
    process->close();
}

void Form::on_pushButton_clicked()
{
    // Обработка кнопки "Сформировать"

    QString org_name    = ui->lineEdit->text();
    QString okud        = ui->lineEdit_2->text();
    QString okpo        = ui->lineEdit_3->text();
    QString num_doc     = ui->lineEdit_4->text();
    QString num_order   = ui->lineEdit_5->text();
    QString count       = ui->lineEdit_6->text();
    QString create_date = ui->dateEdit->text();
    QString period_from = ui->dateEdit_2->text();
    QString period_to   = ui->dateEdit_3->text();
    QString order_from  = ui->dateEdit_4->text();

    replace_word_in_doc("shtatnoerasp.docx", org_name, "{orgname}");
    replace_word_in_doc("shtatnoerasp.docx", okud, "{code}");
    replace_word_in_doc("shtatnoerasp.docx", okpo, "{coder}");
    replace_word_in_doc("shtatnoerasp.docx", num_doc, "{docnum}");
    replace_word_in_doc("shtatnoerasp.docx", num_order, "{k}");
    replace_word_in_doc("shtatnoerasp.docx", count, "{workernum}");
    replace_word_in_doc("shtatnoerasp.docx", create_date, "{docdate}");
    replace_word_in_doc("shtatnoerasp.docx", period_from, "{firstdate}");
    replace_word_in_doc("shtatnoerasp.docx", period_to, "{seconddate}");
    replace_word_in_doc("shtatnoerasp.docx", order_from, "{third}");

    close();
}


void Form::on_pushButton_2_clicked()
{
    // Обработка кнопки "Отменить"

    close();
}

