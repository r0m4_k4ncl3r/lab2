'''
Вспомогательная утилита для основной программы, нужна для того, чтобы 
подставить слово в нужную часть документа
'''

from docx import Document # билиотека для работы с документами
from docx.shared import Pt # функция для размера текста
import sys # данная библиотека нужна, чтобы получить аргументы из cmd

def main() -> None:
    '''
    Основная функция программы, в ней происходит сначало открытие документа,
    далее происходит замена слова в документе на слово, 
    которое нужно подставить
    '''

    name_document = sys.argv[1].replace('"', '') # Имя документа
    word = sys.argv[2].replace('"', '') # Слово на которое нужно заменить
    word_in_doc = sys.argv[3].replace('"', '') # Заменяемое слово в документе

    document = Document(name_document) # Открываем документ

    # Создание стиля
    style = document.styles['Normal']
    font  = style.font
    font.name = "Times New Roman"
    font.size = Pt(10)

    # Проходимся по всем строкам документа, кроме таблиц
    for paragraph in document.paragraphs:
        # Проверяем есть ли слово в тексте строки
        if word_in_doc in paragraph.text:
            # Замена слова на нужное
            paragraph.text = str(paragraph.text).replace(word_in_doc, word)
            paragraph.style = document.styles['Normal']
            document.save(name_document)
            return None

    #Проходимся по всем таблицам и строкам в таблицк
    for table in document.tables:
        for row in table.rows:
            for cell in row.cells:
                # Проверяем есть ли слово в тексте строки
                if word_in_doc in cell.text:
                    # Замена слова на нужное
                    cell.text = str(cell.text).replace(word_in_doc, word)
                    cell.style = document.styles['Normal']
                    document.save(name_document)
                    return None

if __name__ == "__main__":
    main()
